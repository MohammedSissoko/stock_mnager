package com.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class LigneCommandeClient implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	private Long idlignecmdeclient;
	
	
	@ManyToOne
	@JoinColumn(name=" idArticle")
	private Article article;
	
	
	@ManyToOne
	@JoinColumn(name=" idCommandeclient")
	private CommandeClient commandeClient;

	public Long getIdlignecmdeclient() {
		return idlignecmdeclient;
	}

	public void setIdlignecmdeclient(Long idlignecmdeclient) {
		this.idlignecmdeclient = idlignecmdeclient;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}
	

}
