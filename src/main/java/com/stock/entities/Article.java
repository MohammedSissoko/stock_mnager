package com.stock.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "article")
public class Article implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idArticle;

	private String codearticle;
	
	private String designation;
	
	private BigDecimal prixunitaireHT;
	
	private BigDecimal tauxTva;
	
	private BigDecimal prixunitaireTTC;
	
	private String photo;
	 
	public Article() {
		// TODO Auto-generated constructor stub
	}
	
	
	@ManyToOne
	@JoinColumn(name="idCategorie")
	private Categorie categorie;

	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}

	public String getCodearticle() {
		return codearticle;
	}

	public void setCodearticle(String codearticle) {
		this.codearticle = codearticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixunitaireHT() {
		return prixunitaireHT;
	}

	public void setPrixunitaireHT(BigDecimal prixunitaireHT) {
		this.prixunitaireHT = prixunitaireHT;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixunitaireTTC() {
		return prixunitaireTTC;
	}

	public void setPrixunitaireTTC(BigDecimal prixunitaireTTC) {
		this.prixunitaireTTC = prixunitaireTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
    
	
}
