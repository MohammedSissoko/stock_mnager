package com.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@SuppressWarnings("serial")
@Entity
public class LigneCommandeFournisseur  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idligneCmdefour;
	
	@ManyToOne
	@JoinColumn(name=" idArticle")
	private Article article;
	
	
	@ManyToOne
	@JoinColumn(name=" idCommandefour")
	private CommandeFournisseur commandeFournisseur;

	public Long getIdligneCmdefour() {
		return idligneCmdefour;
	}

	public void setIdligneCmdefour(Long idligneCmdefour) {
		this.idligneCmdefour = idligneCmdefour;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
	
   
}
