package com.stock.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
public class Vente  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idvente;

	private String code;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datevente;
	
	@OneToMany(mappedBy="vente")
	private List<LigneVente> ligneventes;
	
	public Long getIdvente() {
		return idvente;
	}

	public void setIdvente(Long idvente) {
		this.idvente = idvente;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDatevente() {
		return datevente;
	}

	public void setDatevente(Date datevente) {
		this.datevente = datevente;
	}

	public List<LigneVente> getLigneventes() {
		return ligneventes;
	}

	public void setLigneventes(List<LigneVente> ligneventes) {
		this.ligneventes = ligneventes;
	}
	
   
}
