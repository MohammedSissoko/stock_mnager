package com.stock.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
public class MvtStock  implements Serializable{
	
	private static final int ENTREE=1;
	private static final int SORTIE=2;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idmvtstock;

	@Temporal(TemporalType.TIMESTAMP)
	private Date datemvt;
	 
	private BigDecimal qte;
	
	private int typeMvt;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	public Long getIdmvtstock() {
		return idmvtstock;
	}

	public void setIdmvtstock(Long idmvtstock) {
		this.idmvtstock = idmvtstock;
	}

	public Date getDatemvt() {
		return datemvt;
	}

	public void setDatemvt(Date datemvt) {
		this.datemvt = datemvt;
	}

	public BigDecimal getQte() {
		return qte;
	}

	public void setQte(BigDecimal qte) {
		this.qte = qte;
	}

	public int getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
   
}
